package org.example.exercicios;

public class Inferencia {

    //Não podemos criar variáveis com o identificador var fora de funções
    public static void main(String[] args) {
        String nome = "Rafael";
        var nomeVar = "Rafael versão var";
        // Apesar de não tiparmos diretamente, o java tipa sozinho, após a tipagem do java o tipo dessa variavel
        // se torna imutável, apenas o tipo é IMUTAVEL, o seu valor continua sendo mutável a não ser que ela seja
        // um final.

        System.out.println(nome);
        System.out.println(nomeVar);

        // var também não pode não ser criada e não inicializada, como o exemplo:
        // var d;
        // assim o java não consegue associar os tipos
    }

}
