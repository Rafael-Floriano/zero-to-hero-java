package org.example.exercicios;

import java.util.logging.Logger;

public class ConversorDeTemperatura {
    // (F - 32) x 5/9 = c
    // A regra é que apenas o F e C devem ser variáveis
    public static void main(String[] args) {
        Logger logger = Logger.getLogger("log test");
        final double FATOR = 5.0/9.0;
        final double AJUSTE = 32;
        double fahrenheit = 326;
        double celsius = 0;

        celsius = (fahrenheit - AJUSTE) * FATOR;
        final String MENSSAGE = "Resultado da conversão de " + fahrenheit + " fahrenheit para Celsius é de "+celsius+" Celsius";
        logger.info(MENSSAGE);
    }
}
