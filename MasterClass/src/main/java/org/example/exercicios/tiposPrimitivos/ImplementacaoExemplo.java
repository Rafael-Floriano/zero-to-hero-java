package org.example.exercicios.tiposPrimitivos;

public class ImplementacaoExemplo {

//    Esses são todos os tipos primitivos númericos inteiros:
    byte valorEmByte = 127; // armazena 1 byte (8 bits)
    short valorEmShort = 6465; // armazena 2 bytes (16 bits)
    int valorEmInt = 122654564; // armazena 4 bytes (32 bits)
    long valorEmLong = 65664654; // armazena 8 bytes (64 bits)

//    Esses são os tipos primitivos númericos com ponto flutuante:
    float valorEmFloa = 2; // armazena 4 byte (32 bits)
    double valorEmDouble = 2.3; // armazena 8 byte (64 bits)

    // Regra número inteiro é um int até que se prove ao contrário e todo numero de ponto flutuante é um
    // double até que isso seja contestado

    // outros tipos:

    char valorEmChar = 'o'; // é o char que recebe valor com apenas aspas simples
    boolean valorEmBoolean = false; // por padrão seu valor é false, aceita apenas true ou false

}
