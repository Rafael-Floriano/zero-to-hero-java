package org.example.exercicios.tiposPrimitivos;

public class TiposPrimitivos {
    public static void main(String[] args) {
        // Informação de um funcionário

        // Tipos númericos inteiros
        byte anosDeEmpresa = 23;
        short numeroDeVoos = 564;
        int id = 5462;
        long pontosAcumulados = 3_234_854_356l;

        // Tipos númericos reais
        float salario = 1_446f;
        double vendasAcumuladas = 2_562_642.25;

        // Tipo booleano
        boolean estaDeFerias = false; // true

        // Tipo caractere
        char status = 'A'; // A - ativo

        // Dias de empresa
        System.out.println(anosDeEmpresa * 365);
        // Número de viagens
        System.out.println(numeroDeVoos / 2);
        // Pontos acumulados
        System.out.println(pontosAcumulados / vendasAcumuladas);
        // Salário do funcionário
        System.out.println("O funcionário com o id: " + id + " ganha -> " + salario);
        // Está de ferias?
        System.out.println("Ferias? " + estaDeFerias + " e está com o status: " + status);
    }
}
